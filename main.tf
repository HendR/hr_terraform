##################################################################################
# PROVIDERS
##################################################################################

provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region     = var.region
}
module "vpc" {
  source = "./modules/vpc"
  env_name = local.env_name
  common_tags = local.common_tags
  network_address_space = var.network_address_space[terraform.workspace]
  priv_subnet_count = var.priv_subnet_count[terraform.workspace]
  pub_subnet_count = var.pub_subnet_count[terraform.workspace]
  billing_code_tag = var.billing_code_tag
  }

module "lb" {
   source = "./modules/lb"
   env_name = local.env_name
   vpc_id = module.vpc.vpc_output.id
   pub_subnet = module.vpc.vpc_pub_subnet_ids
}

module "auto-scalling" {
  source                 = "./modules/auto-scalling"
  env_name               = local.env_name
  common_tags            = local.common_tags
  vpc_id                 = module.vpc.vpc_output.id
  lb_security_group      = [module.lb.lb_sg_id]
  bastion_security_group = [module.bastion.bastion_sg_id]
  ami_id                 = var.ami_id
  instance_type          = var.priv_instance_size[terraform.workspace]
  key_name               = var.key_name
  vpc_zone_identifier    = module.vpc.vpc_priv_subnet_ids
  target_group_arns      = [module.lb.aws_lb_target_group_asg.arn]
  min_size               = var.min_size[terraform.workspace]
  max_size               = var.max_size[terraform.workspace]
  desired_capacity       = var.desired_capacity[terraform.workspace]



}

module "bastion" {
  source            = "./modules/bastion"
  vpc_id            = module.vpc.vpc_output.id
  env_name          = local.env_name
  common_tags       = local.common_tags
  ami_id            = var.ami_id
  instance_size     = var.bastion_instance_size
  subnet_id         = module.vpc.vpc_pub_subnet_ids[0]
  key_name          = var.key_name
}
