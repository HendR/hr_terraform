# Create SG for ALB ####
 resource "aws_security_group" "lb-sg" {
   name   = "lb-sg"
   vpc_id = var.vpc_id
# 
 ## Allow HTTP from anywhere
   ingress {
     from_port   = 80
     to_port     = 80
     protocol    = "tcp"
     cidr_blocks = ["0.0.0.0/0"]
   }  
# 
  ##allow all outbound
   egress {
     from_port   = 0
     to_port     = 0
     protocol    = "-1"
     cidr_blocks = ["0.0.0.0/0"]
   }

 tags = merge(var.common_tags, { Name = "${var.env_name}-lb-sg" })

}

resource "aws_lb" "alb" {
    name = "${var.env_name}-alb"
    load_balancer_type = "application"
    subnets =  var.pub_subnet
    security_groups = [aws_security_group.lb-sg.id]
    tags = merge(var.common_tags, { Name = "${var.env_name}-lb"})

}

#######
resource "aws_lb_listener" "http" {
    load_balancer_arn = aws_lb.alb.arn
    port = 80
    protocol = "HTTP"
# By default, return a simple 404 page
    default_action {
        type = "fixed-response"
    fixed_response {
        content_type = "text/plain"
        message_body = "404: page not found"
        status_code  = 404
                }
           }
}

resource "aws_lb_target_group" "asg" {
    name = "asg"
    port = 80
    protocol = "HTTP"
    vpc_id = var.vpc_id
    health_check {
        path        = "/"
        protocol     = "HTTP"
        matcher     = "200"
        interval    = 15
        timeout     =  3
        healthy_threshold = 2
        unhealthy_threshold = 2
    }
}
resource "aws_lb_listener_rule" "asg" {
  listener_arn = aws_lb_listener.http.arn
  priority     = 100

  condition {
    field  = "path-pattern"
    values = ["*"]
  }

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.asg.arn
  }
}