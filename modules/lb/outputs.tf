output "aws_lb_target_group_asg" {
    value = aws_lb_target_group.asg
}

output "lb_sg_id" {
    value = aws_security_group.lb-sg.id
}
