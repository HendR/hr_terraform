variable "env_name" {
    default={}
 } 
variable "common_tags" {
    default= {}
}
variable "ami_id" {
    default= {}
}

variable "instance_size" {
    default= {}
}

variable "subnet_id" {
    default = {}
}

variable "key_name" {
    default = {}
}

variable "vpc_id" {
    default= {}
}