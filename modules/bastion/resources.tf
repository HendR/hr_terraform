### Create SG
resource "aws_security_group" "bastion-sg" {
  name   = "bastion-sg"
  vpc_id = var.vpc_id

  #Allow SSH 
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["163.172.209.212/32"]

  }
  
  #allow all outbound
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

tags = merge(var.common_tags, { Name = "${var.env_name}-bastion-sg" })

}
### Create EIP for bastion instance ####
resource "aws_eip" "bastion_eip" {
  vpc = true
}
#############################Create Instance #################
resource "aws_instance" "bastion" {
  ami                    = var.ami_id
  instance_type          = var.instance_size
  subnet_id              = var.subnet_id
  vpc_security_group_ids = [aws_security_group.bastion-sg.id]
  key_name               = var.key_name

tags = merge(var.common_tags, { Name = "${var.env_name}-bastion" })
}

resource "aws_eip_association" "bastion_eip_assoc" {
  instance_id   = aws_instance.bastion.id
  allocation_id = aws_eip.bastion_eip.id
}