## ### Create SG for Launch_configuretion ####
 resource "aws_security_group" "lc-sg" {
 name   = "lc-sg"
 vpc_id = var.vpc_id
### 
   ingress {
     from_port   = 80
     to_port     = 80
     protocol    = "tcp"
     security_groups = var.lb_security_group
     }  

#  Allow ssh from bastion
   ingress {
     from_port   = 22
     to_port     = 22
     protocol    = "tcp"
     security_groups = var.bastion_security_group
     }
 ##allow all outbound
 egress {
   from_port   = 0
   to_port     = 0
   protocol    = "-1"
   cidr_blocks = ["0.0.0.0/0"]
 }

  tags = merge(var.common_tags, { Name = "${var.env_name}-lc-sg" })

}
 resource "aws_launch_configuration" "launch_config" {
  name            = "lc"
  image_id        = var.ami_id
  instance_type   = var.instance_type
  security_groups = [aws_security_group.lc-sg.id]
  user_data       = data.template_file.user_data.rendered
  key_name        = var.key_name
  lifecycle {
  create_before_destroy = true
  }

}

  data "template_file" "user_data" {
  template = file("${path.module}/user-data.sh")

}

resource "aws_autoscaling_policy" "asgp" {
  name                              = "asgp"
  policy_type                       = "TargetTrackingScaling"
  estimated_instance_warmup               = 30
  autoscaling_group_name = aws_autoscaling_group.asg.name
  target_tracking_configuration {
  predefined_metric_specification {
  predefined_metric_type = "ASGAverageCPUUtilization"
  }

  target_value = 50.0
  }
}

resource "aws_autoscaling_group" "asg" {    
 name                 = "asg"
 launch_configuration = aws_launch_configuration.launch_config.name
 vpc_zone_identifier  = var.vpc_zone_identifier
 target_group_arns    = var.target_group_arns
 health_check_type    = "ELB"
 default_cooldown     = 30
 min_size = var.min_size
 desired_capacity = var.desired_capacity 
 max_size = var.max_size

        tags = [
    {
      key                 = "BillingCode"
      value               = "AC3768"
      propagate_at_launch = true
    },
    {
      key                 = "Environment"
      value               = "test"
      propagate_at_launch = true
    },
      {
      key                 = "Name"
      value               = "test-asg"
      propagate_at_launch = true
    },
  ]
#  
}