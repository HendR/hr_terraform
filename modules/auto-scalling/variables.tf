variable "common_tags" {
    default= {}
}

variable "env_name" {
    default= {}
 } 
variable "vpc_id" {
    default= {}
}

 variable "lb_security_group" {
     type= list(string)
        default= []
 }
  variable "bastion_security_group" {
     type= list(string)
        default= []
 }

 variable "ami_id" {
    default= {}
}

variable "instance_type" {
    default= {}
}

variable "key_name" {
    default= {}
}

  variable "vpc_zone_identifier" {
     type= list(string)
        default= []
 }

   variable "target_group_arns" {
     type= list(string)
        default= []
 }
 variable "min_size" {
     default= {}
 }
 variable "max_size" {
     default= {}
 }
 variable "desired_capacity" {
     default= {}
 }