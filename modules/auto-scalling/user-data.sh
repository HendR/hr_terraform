#!/bin/bash
sudo apt  update && sudo apt install nginx -y
sudo rm -rf /var/www/html/index.nginx-debian.html
cat > /var/www/html/index.html <<EOF
<h1>Hello, World</h1>
<p>`hostname` </p>
EOF
sudo systemctl restart nginx
sudo systemctl enable nginx
