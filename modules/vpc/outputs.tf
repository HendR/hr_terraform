output "vpc_output" {
    value = aws_vpc.vpc
}
 output "vpc_pub_subnet_ids" {
     value = aws_subnet.pub_subnet[*].id
 }

 output "vpc_priv_subnet_ids" {
     value = aws_subnet.priv_subnet[*].id
 }