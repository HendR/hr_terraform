############## This module create 
############## 1- VPC
############## 2- Public and Private subnets
############## 3- IGW
############## 4- NGW
############## 5- Routing tables for public and private subnets

#################################################################################
# DATA
##################################################################################
###### data da btb2a 7aga mwgoda 3la aws 
#############https://www.terraform.io/docs/providers/aws/d/availability_zones.html
###################################################################################
data "aws_availability_zones" "available" {}
#####################################################################################

##1-Create VPC

resource "aws_vpc" "vpc" {
 cidr_block= var.network_address_space
 tags= merge(var.common_tags, {Name="${var.env_name}-vpc"})
}
########################################################################################
###2-Create IGW

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id
  tags = merge(var.common_tags, {Name = "${var.env_name}"})
}
###########################################################################################
###3- Create Public Subnets
### Hycreate AZ 3la 7asab el subnets kol wa7ed fi az like az1,az2,az3
#### Hycreate public subnets 3la 3addhom 

resource "aws_subnet" "pub_subnet" {
  count                       = var.pub_subnet_count
  cidr_block                  = cidrsubnet(var.network_address_space, 8, count.index)
  vpc_id                      = aws_vpc.vpc.id
  map_public_ip_on_launch     = true
  availability_zone           = element(data.aws_availability_zones.available.names, count.index)
  tags = merge(var.common_tags, { Name = "${var.env_name}-pub-subnet${count.index + 1}" })
 }
 #############################################################################################
 ###4- Create Private Subnet
 ### Hycreate AZ 3la 7asab el subnets kol wa7ed fi az like az1,az2,az3
 #### Hycreate subnet ba3d 3add el private ex. 5,6,7 (10.0.5.0/24),(10.0.6.0/24)

 resource "aws_subnet" "priv_subnet" {
   count                       = var.priv_subnet_count
   cidr_block                  = cidrsubnet(var.network_address_space, 8, var.pub_subnet_count+count.index)
   vpc_id                      = aws_vpc.vpc.id
   map_public_ip_on_launch     = true
   availability_zone           = element(data.aws_availability_zones.available.names, count.index)
   tags = merge(var.common_tags, { Name = "${var.env_name}-priv-subnet${count.index + 1}" })
 }
# #################################################################################################
######5- Routing ###
#### ### a-Create public Router

 resource "aws_route_table" "rtb_pub" {
   vpc_id = aws_vpc.vpc.id
     route {
     cidr_block = "0.0.0.0/0"
     gateway_id = aws_internet_gateway.igw.id
   }
   tags = merge(var.common_tags, { Name = "${var.env_name}-rtb_pub" })

 }
 resource "aws_route_table_association" "rta-subnet_pub" {
   count            = var.pub_subnet_count
   subnet_id        = aws_subnet.pub_subnet[count.index].id
   route_table_id   = aws_route_table.rtb_pub.id

 }

# ##################3
# #### b-Create Private routing
# ##### 1- Create EIP ####
# ## https://www.terraform.io/docs/providers/aws/r/eip.html

 resource "aws_eip" "eip" {
   vpc              = true
 }
# ### Create nat gatewat ###
####### Will be Always in the first public subnet 
 resource "aws_nat_gateway" "ngw" {
   allocation_id = aws_eip.eip.id
   subnet_id     =  aws_subnet.pub_subnet[0].id
   tags = merge(var.common_tags, { Name = "${var.env_name}-ngw" })  
    }
# ###########create private routing table
 resource "aws_route_table" "rtb_priv" {

   vpc_id = aws_vpc.vpc.id
     route {
     cidr_block = "0.0.0.0/0"
     gateway_id = aws_nat_gateway.ngw.id
   }
   tags = merge(var.common_tags, { Name = "${var.env_name}-rtb_priv" })
 }

 resource "aws_route_table_association" "rta-subnet_priv" {
   count            = var.priv_subnet_count
   subnet_id        = aws_subnet.priv_subnet[count.index].id
   route_table_id   = aws_route_table.rtb_priv.id

 }







