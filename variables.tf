## Locals##
##########################################
locals {
env_name = lower(terraform.workspace)
common_tags = {
    BillingCode = var.billing_code_tag
    Environment = local.env_name
    }
#s3_bucket_name = "${var.bucket_name_prefix}-${local.env_name}-${random_integer.rand.result}"

}

#####################################
## Variables
#####################################
variable "aws_access_key" {}

variable "aws_secret_key" {}
variable "region" {
    default= "eu-central-1"
}
variable "network_address_space" {
    type = map(string)
}

variable "priv_subnet_count" {
    type = map(number)

}
variable "pub_subnet_count" {
    type = map(number)

}
variable "billing_code_tag" {}
#####Create instance ############
variable "ami_id" {}

variable "priv_instance_size" {
  type = map(string)
}

variable "min_size" {
    type = map(number)
}
variable "max_size" {
    type = map(number)
}
variable "desired_capacity" {
    type = map(number)
}

variable "key_name" {}

variable "priv_instance_count" {}
#####
variable "pub_instance_size" {
  type = map(string)
}
variable "pub_instance_count" {}


variable "bastion_instance_size" {}

#  variable "min_size" {}
#  variable "max_size" {}
#  variable "desired_capacity" {}
# 

