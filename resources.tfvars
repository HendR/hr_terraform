### LOCALS###
billing_code_tag = ""
##########
aws_access_key = ""
aws_secret_key = ""
network_address_space = {
   dev="10.0.0.0/16"
   test="10.1.0.0/16"
   prod= "10.2.0.0/16"
}

priv_subnet_count = {
    dev=3
    test=3
    prod=3
}
pub_subnet_count = {
    dev=3
    test=3
    prod=3

}
#########
ami_id = "ami-0b418580298265d5c"

priv_instance_size = {
  dev = "t2.micro"
  test = "t2.micro"
  prod = "t2.medium"
}

#private_key_path = "/home/hend/Downloads/.pem"
key_name = ""
priv_instance_count = 1
pub_instance_count = 1

pub_instance_size = {
  dev = "t2.micro"
  test = "t2.micro"
  prod = "t2.medium"
}
min_size = {
  dev = 2
  test = 3
  prod = 2
}

max_size = {
  dev = 4
  test = 6
  prod = 2
  onica- = 2
}
desired_capacity = {
  dev = 2
  test = 3
  prod = 2
  onica-test = 2
}

bastion_instance_size = "t2.micro"